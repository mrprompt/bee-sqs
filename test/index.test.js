const expect = require('expect.js');

require('dotenv').config()

describe('SQS Provider', function() {
    beforeEach(function() {
        this.sqs = require(__dirname + '/../src/index');
    });

    afterEach(function() {
        this.sqs = null;
    });

    it('return instance', function(done) {
        expect(this.sqs).to.be.an.Object;

        done();
    });

    describe('createQueue()', function (done) {
        it('createQueue() should return a function', function (done) {
            expect(this.sqs.createQueue).to.be.a.Function;

            done();
        });

        it('createQueue() called with name must be return QueueUrl', function (done) {
            this.sqs.createQueue('foo', {}, function(err, result) {
                expect(err).to.be.Null;
                expect(result).not.to.be.Empty;
                expect(result.ResponseMetadata).not.to.be.Empty;
                expect(result.ResponseMetadata.RequestId).not.to.be.Empty;
                expect(result.QueueUrl).not.to.be.Empty;

                done();
            });
        });

        it('createQueue() called without name throws Error', function (done) {
            this.sqs.createQueue(null, {}, function(err, result) {
                expect(err).to.be.a(Error);
                expect(err.message).to.be.equal(`Missing required key 'QueueName' in params`);
                expect(result).to.be.Null;

                done();
            });
        });
    });

    describe('deleteQueue()', function (done) {
        it('deleteQueue() should return a function', function (done) {
            expect(this.sqs.deleteQueue).to.be.a.Function;

            done();
        });

        it('deleteQueue() called with unknown queue thows Error', function (done) {
            this.sqs.deleteQueue('fxoo', function (err, result) {
                expect(err).to.be.Null;
                expect(err.UnknownEndpoint).not.to.be.Empty;
                expect(result).to.be.Empty;

                done();
            });
        });

        it('deleteQueue() called with valid queue must be return ok', function (done) {
            const sqs = this.sqs;
            const queue = Math.random().toString(36).substr(2, 5);

            sqs.createQueue(queue, {}, function (err, result) {
                sqs.deleteQueue(result.ResponseMetadata.QueueUrl, function (err2, result2) {
                    expect(err2).to.be.Null;
                    expect(result2).not.to.be.Empty;
                    expect(result.ResponseMetadata.RequestId).not.to.be.Empty;

                    done();
                });
            });
        });

        it('deleteQueue() called without name throws Error', function (done) {
            this.sqs.deleteQueue(null, function (err, result) {
                expect(err).to.be.a(Error);
                expect(err.message).to.be.equal(`Missing required key 'QueueUrl' in params`);
                expect(result).to.be.Null;

                done();
            });
        });
    });

    describe('addMessage()', function (done) {
        it('addMessage() should return a function', function (done) {
            expect(this.sqs.addMessage).to.be.a.Function;

            done();
        });
        
        it('addMessage() called with empty message receive valid MessageId', function (done) {
            this.sqs.addMessage({}, function(err, result) {
                expect(err).to.be.Null;
                expect(result).not.to.be.Empty;
                expect(result.ResponseMetadata).not.to.be.Empty;
                expect(result.ResponseMetadata.RequestId).not.to.be.Empty;
                expect(result.MessageId).not.to.be.Empty;

                done();
            });
        });
        
        it('addMessage() called with not empty message receive valid MessageId', function (done) {
            const message = {
                name: 'Foo',
                email: 'foo@bar.bar'
            };

            this.sqs.addMessage(message, function(err, result) {
                expect(err).to.be.Null;
                expect(result).not.to.be.Empty;
                expect(result.ResponseMetadata).not.to.be.Empty;
                expect(result.ResponseMetadata.RequestId).not.to.be.Empty;
                expect(result.MessageId).not.to.be.Empty;

                done();
            });
        });
    });

    describe('deleteMessage()', function(done) {
        it('deleteMessage() should return a function', function(done) {
            expect(this.sqs.deleteMessage).to.be.a.Function;
            
            done();
        });

        it('deleteMessage() called with unknown message handle thows Error', function (done) {
            this.sqs.deleteMessage('fxoo', function (err, result) {
                expect(err).to.be.Null;
                expect(err.UnknownEndpoint).not.to.be.Empty;
                expect(result).to.be.Empty;

                done();
            });
        });

        it('deleteMessage() called with valid queue must be return ok', function (done) {
            const sqs = this.sqs;
            const handle = 'AQEBAFSi1BPiXVhR4wEnAU0HdxwqCCDMDVCMmYgyWfbK9n1ZUZwDljv1AWX'
                + 'nkMa0U0Sw/QbdgElyxsGOYb17gZz/bcX8KFV4eUlJJJ5wchQc4mNneQ6e+pLX/x4rQiF'
                + '8PUprdP8I+S3f1oLTBUu7fO0Vgi5uIo6C8xAR/OwHsVAqrYfhG5jMfbEqvr4vd9t2RiZ'
                + 'hAMHC/cDvJt/oe5ah7xNufDwLtOJiNSuJZ4gwPSXXCeKW+NxlZvfyZpscHJpOtfgwxwD'
                + 'Eo2LJTAe7xaCmqtAsCCgwrLm5fLSYVwoGKXWqmasKN/TtscvOthverXynLmJXyitE5nO'
                + 'GLaoqlZPSRZgay36CftKql4JkssD6z3HEuwoX88QDHAAG0UGTCzRfenrZ9Ll69uGbekb'
                + 'UTBjeg21zoA==';

            sqs.deleteMessage(handle, function (err, result) {
                expect(err).to.be.Null;
                expect(result).not.to.be.Empty;
                expect(result.ResponseMetadata.RequestId).not.to.be.Empty;

                done();
            });
        });

        it('deleteMessage() called without name throws Error', function (done) {
            this.sqs.deleteMessage(null, function (err, result) {
                expect(err).to.be.a(Error);
                expect(err.message).to.be.equal(`Missing required key 'ReceiptHandle\' in params`);
                expect(result).to.be.Null;

                done();
            });
        });
    });

    describe('receiveMessages()', function(done) {
        it('receiveMessages() should return a function', function(done) {
            expect(this.sqs.receiveMessages).to.be.a.Function;
            
            done();
        });

        it('receiveMessages() with default params return array', function(done) {
            this.sqs.receiveMessages(null, function(err, result) {
                expect(err).to.be.Null;
                expect(result).not.to.be.Empty;
                expect(result.ResponseMetadata).not.to.be.Empty;
                expect(result.ResponseMetadata.RequestId).not.to.be.Empty;
                expect(result.Messages).not.to.be.Empty;
                expect(result.Messages).to.be.an.Array;

                done();
            })
        });

        it('receiveMessages() with valid params return array', function(done) {
            this.sqs.receiveMessages(10, function(err, result) {
                expect(err).to.be.Null;
                expect(result).not.to.be.Empty;
                expect(result.ResponseMetadata).not.to.be.Empty;
                expect(result.ResponseMetadata.RequestId).not.to.be.Empty;
                expect(result.Messages).not.to.be.Empty;
                expect(result.Messages).to.be.an.Array;

                done();
            })
        });
    });
});