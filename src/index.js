const AWS = require('aws-sdk');

const settings = {
  aws: {
    config: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID || '',
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || '',
      region: process.env.AWS_DEFAULT_REGION || ''
    },
    sqs: {
      url: process.env.AWS_SQS_URL || '',
    }
  }
}

const SQS = new AWS.SQS({
  accessKeyId: settings.aws.config.accessKeyId,
  secretAccessKey: settings.aws.config.secretAccessKey,
  region: settings.aws.config.region,
});

function createQueue(name, attrs, callback) {
  const params = {
    QueueName: name,
    Attributes: attrs
  };

  SQS.createQueue(params, callback);
};

function deleteQueue(queue, callback) {
  const params = {
    QueueUrl: queue
  };

  SQS.deleteQueue(params, callback);
};

function addMessage(message, callback) {
  const params = {
    MessageBody: JSON.stringify(message),
    QueueUrl: settings.aws.sqs.url,
  };

  SQS.sendMessage(params, callback);
};

function deleteMessage (handle, callback = () => {}) {
  const params = {
    QueueUrl: settings.aws.sqs.url,
    ReceiptHandle: handle,
  };

  SQS.deleteMessage(params, callback);
}

function receiveMessages (limit = 5, callback = () => {}) {
  const params = {
    QueueUrl: settings.aws.sqs.url,
    MaxNumberOfMessages: limit,
  };

  SQS.receiveMessage(params, callback);
}

module.exports = { deleteMessage, receiveMessages, createQueue, deleteQueue, addMessage };