# Bee - SQS Service

Amazon SQS Client.

## Methods

- deleteMessage
- receiveMessages
- createQueue
- deleteQueue
- addMessage

## Install

First, configure AWS configurations

```console
AWS_ACCESS_KEY_ID="<your key>"
AWS_SECRET_ACCESS_KEY="<your secret>"
AWS_DEFAULT_REGION="<your region>"
AWS_SQS_URL="<your sqs url>"
```

## Usage

### Create Queue

```javascript
const sqs = require('bee-sqs');

sqs.createQueue('foo', {}, function(err, result) {
    if (err) return console.error(err);

    console.info(result);
});
```

### Delete Queue

```javascript
const sqs = require('bee-sqs');

const AWS_SQS_URL = 'https://foo.sa-east-1.amazonaws.com/000000/queue-sample'

sqs.deleteQueue(AWS_SQS_URL, function(err, result) {
    if (err) return console.error(err);

    console.info(result);
});
```

### Create Message

```javascript
const sqs = require('bee-sqs');

const customer = {
    name: 'Foo',
    email: 'foo@bar.bar'
};

const message = {
    customer,
    event: 'transaction-report',
    context: 'registration'
};

sqs.addMessage(message, function(err, result) {
    if (err) return console.error(err);

    console.info(result);
});
```

### Delete Message

```javascript
const sqs = require('bee-sqs');

const handle = 'AQEBCFSi9BPxXVhR4wEnAU0HdxwqCCDMDVCMmYgyWfbK9n1ZUZwDljv1AWX'
    + 'nkMa0U0Sw/QbdgElyxsGOYb17gZz/bcX8KFV4eUlJJJ5wchQc4mNneQ6e+pLX/x4rQiF'
    + '8PUprdP8I+S3f1oLTBUu7fO0Vgi5uIo6C8xAR/OwHsVAqrYfhG5jMfbEqvr4vd9t2RiZ'
    + 'hAMHC/cDvJt/oe5ah7xNufDwLtOJiNSuJZ4gwPSXXCeKW+NxlZvfyZpscHJpOtfgwxwD'
    + 'Eo2LJTAe7xaCmqtAsCCgwrLm5fLSYVwoGKXWqmasKN/TtscvOthverXynLmJXyitE5nO'
    + 'GLaoqlZPSRZgay36CftKql4JkssD6z3HEuwoX88QDHAAG0UGTCzRfenrZ9Ll69uGbekb'
    + 'UTBjeg21zoA==';

sqs.deleteMessage(handle, function(err, result) {
    if (err) return console.error(err);

    console.info(result);
});
```

### Receive Messages

```javascript
const sqs = require('bee-sqs');

const MESSAGE_LIMIT = 1000;

sqs.receiveMessages(MESSAGE_LIMIT, function(err, result) {
    if (err) return console.error(err);

    console.info(result);
});
```

## Test

This needs a .env file with AWS credentials and SQS Url.

```console
npm test
```